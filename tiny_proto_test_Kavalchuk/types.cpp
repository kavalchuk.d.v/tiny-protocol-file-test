#include "types.hpp"
#include "utilities.hpp"
#include <charconv>
#include <cmath>
#include <iostream>

template <typename T>
static bool parseSimpleParam(std::string_view dataString,
                             std::string_view parameterName, T minBound,
                             T maxBound, T& r_value)
{
    if (dataString.size() == 0)
    {
        std::cerr << "Cannot parse " << parameterName
                  << ". It must contain number of symbols "
                     "greater than 0. Received: "
                  << dataString.size() << std::endl;
        return false;
    }

    if (!isDigit(dataString))
    {
        std::cerr << "Cannot parse " << parameterName
                  << ". It must contain only digits from 0 to "
                     "9. Received: "
                  << dataString << std::endl;
        return false;
    }

    int receivedValue{};
    {
        const auto resultConverion = std::from_chars(
            dataString.begin(), dataString.end(), receivedValue);

        if (resultConverion.ec != std::errc())
        {
            std::cerr << "Cannot parse " << parameterName << std::endl;
            return false;
        }

        if (receivedValue < minBound || receivedValue > maxBound)
        {
            std::cerr << parameterName << " out of border. It must be between "
                      << minBound << " " << maxBound << std::endl;
            return false;
        }
    }

    r_value = receivedValue;
    return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Date::fillFromStr(std::string_view dataStringDate)
{
    if (dataStringDate.size() != 8)
    {
        std::cerr << "Cannot parse date it must contain 8 symbols. Received: "
                  << dataStringDate.size() << std::endl;
        return false;
    }

    int        receivedDays;
    const auto daySubStr = dataStringDate.substr(0, 2);
    if (!parseSimpleParam(daySubStr, "Date days", 0, 31, receivedDays))
    {
        return false;
    }

    int        receivedMonths;
    const auto monthsSubStr = dataStringDate.substr(2, 2);
    if (!parseSimpleParam(monthsSubStr, "Date months", 0, 12, receivedMonths))
    {
        return false;
    }

    int        receivedYears;
    const auto yearsSubStr = dataStringDate.substr(4, 4);
    if (!parseSimpleParam(yearsSubStr, "Date years", 1900, 3000, receivedYears))
    {
        return false;
    }

    days   = receivedDays;
    months = receivedMonths;
    years  = receivedYears;
    return true;
}

bool Time::fillFromStr(std::string_view dataStringTime)
{
    if (dataStringTime.size() != 6)
    {
        std::cerr << "Cannot parse date it must contain 6 symbols. Received: "
                  << dataStringTime.size() << std::endl;
        return false;
    }

    int        receivedHours;
    const auto hoursSubStr = dataStringTime.substr(0, 2);
    if (!parseSimpleParam(hoursSubStr, "Time hours", 0, 23, receivedHours))
    {
        return false;
    }

    int        receivedMinutes;
    const auto minutesSubStr = dataStringTime.substr(2, 2);
    if (!parseSimpleParam(minutesSubStr, "Time minutes", 0, 59,
                          receivedMinutes))
    {
        return false;
    }

    int        receivedSeconds;
    const auto secondsSubStr = dataStringTime.substr(4, 2);
    if (!parseSimpleParam(secondsSubStr, "Time seconds", 0, 59,
                          receivedSeconds))
    {
        return false;
    }

    hours   = receivedHours;
    minutes = receivedMinutes;
    seconds = receivedSeconds;
    return true;
}

////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] static bool getUnsignedDoubleCoordinateValue(
    std::string_view dataStringCoordinates, const CoordinateType coordinateType,
    double& r_coordinate)
{
    const auto numberParts = splitLineToWords(dataStringCoordinates, '.');

    const size_t lengthOfDegrees =
        (coordinateType == CoordinateType::latitude) ? 2 : 3;

    const std::string type =
        (coordinateType == CoordinateType::latitude) ? "Latitude" : "Longitude";

    const size_t lengthOfMinutes = 2;

    const auto requiredLengthBeforeDot = lengthOfDegrees + lengthOfMinutes;

    if (numberParts.size() != 2)
    {
        std::cerr << "Cannot parse " << type
                  << ". First part must contain delimiter \'.\'" << std::endl;
        return false;
    }

    const auto nonDigitIterator = std::find_if(
        numberParts.begin(), numberParts.end(),
        [](std::string_view numberPart) { return !isDigit(numberPart); });

    if (nonDigitIterator != numberParts.end())
    {
        std::cerr << "Cannot parse " << type
                  << ". It must contain only digits from 0 to "
                     "9. Received: "
                  << *nonDigitIterator << std::endl;
        return false;
    }

    if (numberParts[0].size() != requiredLengthBeforeDot)
    {
        std::cerr << "Cannot parse " << type
                  << ". Degrees and integer part of "
                     "minutes must contain 4 symbols. Received: "
                  << numberParts.front().size() << std::endl;
        return false;
    }

    const auto maxBoundDegrees =
        (coordinateType == CoordinateType::latitude) ? 90 : 180;

    int receivedDegrees{};
    {
        const auto degreesSubStr = numberParts[0].substr(0, lengthOfDegrees);
        if (!parseSimpleParam(degreesSubStr, type + " degrees", 0,
                              maxBoundDegrees, receivedDegrees))
        {
            return false;
        }
    }

    int receivedMinutesIntegerPart{};
    {
        const auto minutesIntegerPartSubStr =
            numberParts[0].substr(lengthOfDegrees, lengthOfMinutes);
        if (!parseSimpleParam(minutesIntegerPartSubStr,
                              type + " integer part of minutes", 0, 59,
                              receivedMinutesIntegerPart))
        {
            return false;
        }
    }

    int        receivedMinutesFractionalPart{};
    const auto receivedFractionMinutesPartSize = numberParts[1].size();
    if (receivedFractionMinutesPartSize > 0)
    {
        const auto minutesFractionalPartSubStr = numberParts[1];
        if (!parseSimpleParam(minutesFractionalPartSubStr,
                              type + " integer part of minutes", 0,
                              std::numeric_limits<int>::max(),
                              receivedMinutesFractionalPart))
        {
            return false;
        }
    }

    const auto fractionalPartDouble =
        static_cast<double>(receivedMinutesFractionalPart) /
        (std::pow(10, receivedFractionMinutesPartSize));

    const auto minutesFloat =
        static_cast<double>(receivedMinutesIntegerPart) + fractionalPartDouble;

    const auto coordinateUnsignedNormalized =
        (static_cast<double>(receivedDegrees) + (minutesFloat / 60.0));

    if ((coordinateUnsignedNormalized < 0.0) ||
        ((coordinateUnsignedNormalized - maxBoundDegrees) >
         std::numeric_limits<double>::epsilon()))
    {
        std::cerr << "Cannot parse " << type
                  << ". Normalized unsigned double value out of border. It "
                     "must be between "
                  << 0 << " " << maxBoundDegrees << std::endl;
        return false;
    }

    r_coordinate = coordinateUnsignedNormalized;

    return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Latitude::getSignFromDirection(std::string_view dataStringDirection,
                                    double&          r_sign)
{
    if (dataStringDirection.size() != 1)
    {
        std::cerr << "Cannot parse latitude. Direction must contain 1 symbols. "
                     "Received: "
                  << dataStringDirection.size() << std::endl;
        return std::numeric_limits<double>::infinity();
    }

    switch (dataStringDirection[0])
    {
        case 'N':
            r_sign = +1.0;
            return true;
        case 'S':
            r_sign = -1.0;
            return true;
        default:
            std::cerr << "Cannot parse latitude. Direction must be N or S. "
                         "Reseived: "
                      << dataStringDirection << std::endl;
            return false;
    }
}

bool Latitude::fillFromStr(std::string_view dataStringCoordinates,
                           std::string_view dataStringDirection)
{
    double sign{};
    if (!getSignFromDirection(dataStringDirection, sign))
    {
        return false;
    }

    double unsignedCoordinateValue{};
    if (!getUnsignedDoubleCoordinateValue(dataStringCoordinates,
                                          CoordinateType::latitude,
                                          unsignedCoordinateValue))
    {
        return false;
    }

    coordinateNormalized = sign * unsignedCoordinateValue;
    return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Longitude::getSignFromDirection(std::string_view dataStringDirection,
                                     double&          r_sign)
{
    if (dataStringDirection.size() != 1)
    {
        std::cerr << "Cannot parse latitude. Direction must contain 1 symbols. "
                     "Received: "
                  << dataStringDirection.size() << std::endl;
        return std::numeric_limits<double>::infinity();
    }

    switch (dataStringDirection[0])
    {
        case 'E':
            r_sign = +1.0;
            return true;
        case 'W':
            r_sign = -1.0;
            return true;
        default:
            std::cerr << "Cannot parse latitude. Direction must be N or S. "
                         "Reseived: "
                      << dataStringDirection << std::endl;
            return false;
    }
}

bool Longitude::fillFromStr(std::string_view dataStringCoordinates,
                            std::string_view dataStringDirection)
{
    double sign{};
    if (!getSignFromDirection(dataStringDirection, sign))
    {
        return false;
    }

    double unsignedCoordinateValue{};
    if (!getUnsignedDoubleCoordinateValue(dataStringCoordinates,
                                          CoordinateType::longitude,
                                          unsignedCoordinateValue))
    {
        return false;
    }

    coordinateNormalized = sign * unsignedCoordinateValue;
    return true;
}

///////////////////////////////////////////////////////////////////////////////

bool Speed::fillFromStr(std::string_view dataStringSpeed)
{
    return parseSimpleParam(dataStringSpeed, "Speed", 0, 10000, speed);
}

bool Course::fillFromStr(std::string_view dataStringCourse)
{
    return parseSimpleParam(dataStringCourse, "Speed", 0, 360, course);
}

bool Height::fillFromStr(std::string_view dataStringHeight)
{
    return parseSimpleParam(dataStringHeight, "Height", -12000, 30000, height);
}

bool Satellites::fillFromStr(std::string_view dataStringSatelites)
{
    return parseSimpleParam(dataStringSatelites, "Satelites", 0, 200,
                            satellites);
}

std::ostream& operator<<(std::ostream& out, Date date)
{
    out << "Date: " << date.days << " days, " << date.months << " months, "
        << date.years << " years;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Time time)
{
    out << "Time: " << time.hours << " hours, " << time.minutes << ", minutes "
        << time.seconds << " seconds;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Latitude latitude)
{
    out << "Latitude normalized coordinate: " << latitude.coordinateNormalized
        << " degrees;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Longitude longitude)
{
    out << "Longitude normalized coordinate: " << longitude.coordinateNormalized
        << " degrees;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Speed speed)
{
    out << "Speed: " << speed.speed << " m/s;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Course course)
{
    out << "Course: " << course.course << " degrees;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Height height)
{
    out << "Height: " << height.height << " m;\n";
    return out;
}

std::ostream& operator<<(std::ostream& out, Satellites satellites)
{
    out << "Number of available satellites: " << satellites.satellites << ";\n";
    return out;
}

#pragma once
#include "types.hpp"
#include <iosfwd>
#include <string>

struct Packet
{
    enum class Type
    {
        SD,
        M,
        err,
    };

    virtual ~Packet() {}

    Type                              type{};
    [[nodiscard]] static Packet::Type getPacketTypeFromLine(
        std::string_view line);
    [[nodiscard]] static bool  checkAndDropEndOfLine(std::string_view& r_line);
    [[nodiscard]] virtual bool fillPacketFromStr(
        std::string_view dataString)                          = 0;
    [[nodiscard]] virtual bool printPacket(std::ostream& out) = 0;
};

struct PacketSd : public Packet
{
    Date       date;
    Time       time;
    Latitude   latitude;
    Longitude  longitude;
    Speed      speed;
    Course     course;
    Height     height;
    Satellites satellites;
    ~PacketSd() override {}
    [[nodiscard]] bool fillPacketFromStr(std::string_view dataString) override;
    [[nodiscard]] bool printPacket(std::ostream& out) override;
};

struct PacketM : public Packet
{
    std::string data;
    ~PacketM() override {}
    [[nodiscard]] bool fillPacketFromStr(std::string_view dataString) override;
    [[nodiscard]] bool printPacket(std::ostream& out) override;
};

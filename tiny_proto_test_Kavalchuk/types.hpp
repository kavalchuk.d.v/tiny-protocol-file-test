#pragma once
#include <iosfwd>
#include <string_view>

struct Date
{
    int                years{};
    int                months{};
    int                days{};
    [[nodiscard]] bool fillFromStr(std::string_view dataStringDate);
};

struct Time
{
    int                hours{};
    int                minutes{};
    int                seconds{};
    [[nodiscard]] bool fillFromStr(std::string_view dataStringTime);
};

enum class CoordinateType
{
    latitude,
    longitude,
};

struct Latitude
{
    double             coordinateNormalized{};
    [[nodiscard]] bool fillFromStr(std::string_view dataStringCoordinates,
                                   std::string_view dataStringDirection);

private:
    [[nodiscard]] bool getSignFromDirection(
        std::string_view dataStringDirection, double& r_sign);
};

struct Longitude
{
    double             coordinateNormalized{};
    [[nodiscard]] bool fillFromStr(std::string_view dataStringCoordinates,
                                   std::string_view dataStringDirection);

private:
    [[nodiscard]] static bool getSignFromDirection(
        std::string_view dataStringDirection, double& r_sign);
};

struct Speed
{
    int                speed;
    [[nodiscard]] bool fillFromStr(std::string_view dataStringSpeed);
};

struct Course
{
    int                course;
    [[nodiscard]] bool fillFromStr(std::string_view dataStringCourse);
};

struct Height
{
    int                height;
    [[nodiscard]] bool fillFromStr(std::string_view dataStringHeight);
};

struct Satellites
{
    int                satellites;
    [[nodiscard]] bool fillFromStr(std::string_view dataStringSatelites);
};

std::ostream& operator<<(std::ostream& out, Date date);
std::ostream& operator<<(std::ostream& out, Time date);
std::ostream& operator<<(std::ostream& out, Latitude latitude);
std::ostream& operator<<(std::ostream& out, Longitude longitude);
std::ostream& operator<<(std::ostream& out, Speed speed);
std::ostream& operator<<(std::ostream& out, Course course);
std::ostream& operator<<(std::ostream& out, Height height);
std::ostream& operator<<(std::ostream& out, Satellites Satellites);

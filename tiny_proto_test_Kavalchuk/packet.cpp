#include "packet.hpp"
#include "utilities.hpp"
#include <iostream>

Packet::Type Packet::getPacketTypeFromLine(std::string_view r_line)
{
    if (r_line == "SD")
    {
        return Packet::Type::SD;
    }
    else if (r_line == "M")
    {
        return Packet::Type::M;
    }
    return Packet::Type::err;
}

bool Packet::checkAndDropEndOfLine(std::string_view& line)
{
    if (!line.ends_with('\r'))
    {
        std::cerr
            << "Cannot parse current packet. Line end must contain \'\\r\' "
            << std::endl;
        return false;
    }
    line.remove_suffix(1);
    return true;
}

bool PacketSd::fillPacketFromStr(std::string_view dataString)
{

    const auto wordsTypeAndData = splitLineToWords(dataString, ';');

    if (wordsTypeAndData.size() != 10)
    {
        std::cerr << "Cannot parse current packet. Incorrect number of fields. "
                     "Must be 1. Received: "
                  << wordsTypeAndData.size() << std::endl;
        return false;
    }

    auto wordsIterator = wordsTypeAndData.begin();

    if (!date.fillFromStr(*wordsIterator))
    {
        return false;
    }
    if (!time.fillFromStr(*(++wordsIterator)))
    {
        return false;
    }
    ++wordsIterator;
    if (!latitude.fillFromStr(*wordsIterator, *(wordsIterator + 1)))
    {
        return false;
    }
    wordsIterator += 2;
    if (!longitude.fillFromStr(*wordsIterator, *(wordsIterator + 1)))
    {
        return false;
    }
    wordsIterator += 2;
    if (!speed.fillFromStr(*(wordsIterator)))
    {
        return false;
    }
    if (!course.fillFromStr(*(++wordsIterator)))
    {
        return false;
    }
    if (!height.fillFromStr(*(++wordsIterator)))
    {
        return false;
    }
    if (!satellites.fillFromStr(*(++wordsIterator)))
    {
        return false;
    }
    return true;
}

bool PacketM::fillPacketFromStr(std::string_view dataString)
{

    const auto wordsTypeAndData = splitLineToWords(dataString, ';');

    if (wordsTypeAndData.size() != 1)
    {
        std::cerr << "Cannot parse current packet. Incorrect number of fields. "
                     "Must be 1. Received: "
                  << wordsTypeAndData.size() << std::endl;
        return false;
    }

    data = wordsTypeAndData.front();
    return true;
}

static std::ostream& operator<<(std::ostream& out, Packet::Type type)
{
    out << "Type: ";
    switch (type)
    {
        case Packet::Type::M:
            out << "M";
            break;
        case Packet::Type::SD:
            out << "SD";
            break;
        default:
            out << "error type";
    }

    out << ";\n";
    return out;
}

bool Packet::printPacket(std::ostream& out)
{
    out << "Packet:\n" << type;
    return static_cast<bool>(out);
}

bool PacketSd::printPacket(std::ostream& out)
{
    const auto isbasePrintOk = Packet::printPacket(out);
    out << date << time << latitude << longitude << speed << course << height
        << satellites << '\n';
    return static_cast<bool>(out) && isbasePrintOk;
}

bool PacketM::printPacket(std::ostream& out)
{
    const auto isbasePrintOk = Packet::printPacket(out);
    out << "Data: " << data << "\n\n";
    return static_cast<bool>(out) && isbasePrintOk;
}

#pragma once
#include <string_view>
#include <vector>

bool isDigit(std::string_view str);

std::vector<std::string_view> splitLineToWords(std::string_view line,
                                               const char       delimeter);

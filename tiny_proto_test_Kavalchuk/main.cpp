#include "packet.hpp"
#include "utilities.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

int main(int /*argc*/, char* /*argv*/[], char* /*argv_env*/[])
{
    // Documentation doesnt contain any information about certain bounds of
    // degrees, minutes of coordinates and it doesnt specify if fractional part
    // of minutes can contain zero symbols. So I do it how myself think. You can
    // go through the logic in function getUnsignedDoubleCoordinateValue and
    // fill functions

    // Output can be redirected to file with ./gurtam_test_1_Kavalchuk
    // 2>>[file_name]

    const std::string fileName{ "input_data.txt" };
    std::ifstream     inputFile{ fileName, std::ios::binary };
    if (!static_cast<bool>(inputFile))
    {
        std::cerr << "Cannot open input file " << fileName << std::endl;
        return EXIT_FAILURE;
    }

    std::string line;

    while (getline(inputFile, line, '\n'))
    {
        std::string_view viewOfCurrentFile = line;
        std::clog << viewOfCurrentFile << std::endl;
        if (!Packet::checkAndDropEndOfLine(viewOfCurrentFile))
        {
            std::cerr
                << "Cannot parse current packet. Line end must contain \'\\r\' "
                << std::endl;
            continue;
        }
        const auto wordsTypeAndData = splitLineToWords(viewOfCurrentFile, '#');
        const auto numberOfPacketSection = wordsTypeAndData.size() - 1;
        if (numberOfPacketSection != 2)
        {
            std::cerr << "Cannot parse current packet. Number of symbol # in "
                         "one packet must be 2. Recived: "
                      << numberOfPacketSection << std::endl;
            continue;
        }

        Packet::Type type = Packet::getPacketTypeFromLine(wordsTypeAndData[1]);

        std::unique_ptr<Packet> packet;

        switch (type)
        {
            case Packet::Type::SD:
                packet = std::make_unique<PacketSd>();
                break;
            case Packet::Type::M:
                packet = std::make_unique<PacketM>();
                break;
            default:
                std::cerr << "Cannot parse current packet. Incorrect type of "
                             "packet. Must be SD or M. Received: "
                          << wordsTypeAndData[1] << std::endl;
                continue;
        }

        if (!packet->fillPacketFromStr(wordsTypeAndData[2]))
        {
            continue;
        }

        if (!packet->printPacket(std::clog))
        {
            std::cerr << "Error when print packet to clog. Raw packet: " << line
                      << std::endl;
        }
    }

    const auto isCsteamsOk = static_cast<bool>(std::cout) &&
                             static_cast<bool>(std::clog) &&
                             static_cast<bool>(std::cerr);

    return (isCsteamsOk) ? EXIT_SUCCESS : EXIT_FAILURE;
}

#include "utilities.hpp"
#include <cctype>
#include <string_view>

bool isDigit(std::string_view str)
{
    return std::all_of(str.begin(), str.end(), std::isdigit);
}

std::vector<std::string_view> splitLineToWords(std::string_view line,
                                               const char       delimeter)
{
    using namespace std;
    if (line.empty())
    {
        return {};
    }

    size_t countSpaces = std::count(line.begin(), line.end(), delimeter);

    std::vector<size_t> startNextWord;
    startNextWord.reserve(countSpaces + 1);
    startNextWord.push_back(0); // first index

    size_t i = 0;

    auto copySpaceIndex = [&startNextWord, &i, delimeter](char value) {
        ++i;
        if (value == delimeter)
        {
            startNextWord.push_back(i);
        }
    };

    for_each(begin(line), end(line), copySpaceIndex);

    vector<string_view> result;
    result.reserve(countSpaces + 1);

    transform((startNextWord.begin()), --(startNextWord.end()),
              ++(startNextWord.begin()), back_inserter(result),
              [&line](size_t first_index, size_t last_index) {
                  return line.substr(first_index, --last_index - first_index);
              });

    auto last_word = line.substr(startNextWord.back());
    result.push_back(last_word);

    return result;
}

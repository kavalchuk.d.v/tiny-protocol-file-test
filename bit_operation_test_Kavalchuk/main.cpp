#include <bitset>
#include <iomanip>
#include <iostream>

int main(int /*argc*/, char* /*argv*/[], char* /*argv_env*/[])
{
    bool                     isContinue{ true };
    constexpr uint_least32_t defaultInputValue{ 0x5F'AB'FF'01 };
    while (isContinue)
    {
        std::cout << "Type value to get additional parameters (type zero to "
                     "use default): ";
        int inputValue{};
        std::cin >> inputValue;
        if (inputValue == 0)
        {
            inputValue = defaultInputValue;
        }
        if (static_cast<bool>(std::cin))
        {
            const auto param1{ (inputValue & 0x00'00'FF'00) >> 8 };

            const auto param2{ ((inputValue & (0b1 << 7)) >> 7) ^ 0b1 };

            const auto param3Mirrored{ (inputValue & (0b1111 << 17)) >> 17 };

            const auto param3{ ((param3Mirrored & 0b0001) << 3) |
                               ((param3Mirrored & 0b0010) << 1) |
                               ((param3Mirrored & 0b0100) >> 1) |
                               ((param3Mirrored & 0b1000) >> 3) };

            const auto param4{ (inputValue & (0b1 << 17)) >> 13 };

            std::cout << "Input      : " << std::setw(12) << inputValue
                      << " hex: " << std::hex << std::setw(10) << inputValue
                      << std::dec << " bit: " << std::bitset<32>(inputValue)
                      << '\n'

                      << "Parameter 1: " << std::setw(12) << param1
                      << " hex: " << std::hex << std::setw(10) << param1
                      << std::dec << " bit: " << std::bitset<8>(param1) << '\n'

                      << "Parameter 2: " << std::setw(12) << param2
                      << " hex: " << std::hex << std::setw(10) << param2
                      << std::dec << " bit: " << std::bitset<1>(param2) << '\n'

                      << "Parameter 3: " << std::setw(12) << param3
                      << " hex: " << std::hex << std::setw(10) << param3
                      << std::dec << " bit: " << std::bitset<4>(param3) << '\n'

                      << "Parameter 4: " << std::setw(12) << param4
                      << " hex: " << std::hex << std::setw(10) << param4
                      << std::dec << " bit: " << std::bitset<5>(param4) << '\n';
        }
        else
        {
            std::cerr << "Misinput! Try again!" << std::endl;
        }
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        char charContinue;
        std::cout << "Type \"y\" to continue:";
        std::cin >> charContinue;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        isContinue = (charContinue == 'y');
    }

    return (static_cast<bool>(std::cout)) ? EXIT_SUCCESS : EXIT_FAILURE;
}
